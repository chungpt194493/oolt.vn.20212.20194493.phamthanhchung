package hedspi.oolt.labs;

import java.sql.SQLOutput;

public class DigitalVideoDisc {
    private String title;
    private String category;
    private String director;
    private int length;
    private float cost;

    // Xay dung cac phuong thuc

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
            return length;

    }

    public void setLength(int length) {
        if (length >= 0)
            this.length = length;
        else
            this.length=0;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        if(cost>0)
            this.cost = cost;
        else
            this.cost=0.0f;
    }

    //2. Cac phuong thuc khoi tao
    // Nv : Tao vùng nhớ , chứa thông tin của object và thiết lập // gán giá trị cho các thuộc tính của object
    // Đặc điểm của constructor
    // tên của constructor trùng với tên lớp
    // ko có kiểu trả về , ko dùng từ khóa void

    //2.1 : Constructor ko tham số
    public DigitalVideoDisc(){
        this.title = "";
        this.category="";
        this.director="";
        this.length=0;
        this.cost=0.0f;
    }

    //2.2 Constructor 1 tham so
    public DigitalVideoDisc(String title){
        this.title = title;
    }
    // Constructor full tham so
    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        this.title = title;
        this.category = category;
        this.director = director;
        this.length = length;
        this.cost = cost;
    }

    //Cac phuong thuc khac
    // in ra thong tin cua doi tuong DVD
    public void printInfor(){
        System.out.println(" -----------DVD Infor-------------- ");
        System.out.println("Title : " + this.title);
        System.out.println("Category: " + this.category);
        System.out.println("Director: " + this.director);
        System.out.println("Length: "+ this.length);
        System.out.println("Cost :"+ this.cost);
    }
}
