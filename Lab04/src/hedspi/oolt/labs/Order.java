package hedspi.oolt.labs;

import com.sun.security.jgss.GSSUtil;

import java.util.ArrayList;
import java.util.List;

public class Order {
    // khai bao hang : so luogn san pham toi da
    public static final int MAX_NUMBER_ORDERED = 10;
    public static final int MAX_LIMIT_ORDERS = 5;
    //khai bao mang cac doi tuong dvd
    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
    // khai bao thuoc tinh chua so luong phan tu hien co trong dơn hang
    private int qtyOrdered = 0;
    // Xay dung phuong thuc
    private static int nbOrders = 0;
    private MyDate dateOrdered;
    public Order() {
        dateOrdered = new MyDate();
    }

    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

    //Xay dung phuong thuc them 1 dvd vao don hàng
    //Thêm 1 đối tượng vào mảng itemsOrdered
    //
    public void addDigitalVideoDisc (DigitalVideoDisc disc){
        if(this.qtyOrdered == MAX_NUMBER_ORDERED){
            System.out.println("This order is full");
        }else {
            this.itemsOrdered[qtyOrdered] = disc;
            qtyOrdered++;
            System.out.println("This disc has been added ");
            System.out.println(" Total disc " + this.qtyOrdered);
        }
    }

    public void addDigitalVideoDisc(DigitalVideoDisc[] disc) {
        List<DigitalVideoDisc> discList = new ArrayList<>();
        if (qtyOrdered > MAX_NUMBER_ORDERED) {
            System.out.println("Đơn hàng đã đầy!");
        } else {
            for (DigitalVideoDisc digitalVideoDisc : disc) { // duyeejt heets phan tu trong mang disc vao phan tu trung gian digitalVideoDisc
                if (qtyOrdered > MAX_NUMBER_ORDERED) {
                    discList.add(digitalVideoDisc);      // this.add DigitalVideoDisc(dvdList[i])
                    continue;
                }
                itemsOrdered[qtyOrdered] = digitalVideoDisc;
                qtyOrdered++;
            }
        }
        if (discList.size() != 0) {
            System.out.println("Danh sách các mặt hàng không thể thêm.");
            discList.forEach(digitalVideoDisc -> System.out.println(digitalVideoDisc.getTitle()));
        }
    }
    // phuong thuc add ddauf vao 2 tham so
    public void addDigitalVideoDisc(DigitalVideoDisc disc1,DigitalVideoDisc disc2){
        this.addDigitalVideoDisc(disc1);
        this.addDigitalVideoDisc(disc2);
    }


    //Xay dung phuong thuc xoa doi tuong dVD khoi don hang
    public void removeDigitalVideoDisc(DigitalVideoDisc disc){
            if(this.qtyOrdered==0){
                System.out.println(" This order is empty ");
            }else{
                this.itemsOrdered[qtyOrdered]=disc;
                qtyOrdered--;
                System.out.println("this disc has been remove");
                System.out.println("Total disc "+ this.qtyOrdered);
            }
    }

    // Tinh tong gia tri don hang
    public float totalCost(){
        float total = 0.0f;
        for(int i =0 ; i < this.qtyOrdered;i++){
            total+=itemsOrdered[i].getCost();
        }
        return total;
    }

    public static Order createOrder() {
        if (nbOrders >= MAX_LIMIT_ORDERS) {
            System.out.println("Bạn đã đạt đến số lượng đơn đặt hàng tối đa.");
            return null;
        } else {
            nbOrders++;
            return new Order();
        }
    }
}
