package hedspi.oolt.labs;

public class TestPassingParameter {
         public static void main(String[] args) {

        }

    public static void swap(DigitalVideoDisc disc1, DigitalVideoDisc disc2){
             DigitalVideoDisc tmp = new DigitalVideoDisc(disc1.getTitle(), disc1.getCategory(),disc1.getDirector(), disc1.getLength(),disc1.getCost());
             disc2.setTitle(disc1.getTitle());
             disc2.setCategory(disc1.getCategory());
             disc2.setDirector(disc1.getDirector());
             disc2.setLength(disc1.getLength());
             disc2.setCost(disc1.getCost());

             disc1.setTitle(tmp.getTitle());
             disc1.setCategory(tmp.getCategory());
             disc1.setDirector(tmp.getDirector());
             disc1.setLength(tmp.getLength());
             disc1.setCost(tmp.getCost());
    }
    public static void changeTitle(DigitalVideoDisc dvd, String title) {
        String oldTitle = dvd.getTitle();
        dvd.setTitle(title);
        dvd = new DigitalVideoDisc(oldTitle);
    }
}
