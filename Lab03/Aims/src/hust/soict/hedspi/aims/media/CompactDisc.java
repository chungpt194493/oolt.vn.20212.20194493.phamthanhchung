package hust.soict.hedspi.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable ,Comparable {
    private String artist;
    private ArrayList<Track> tracks = new ArrayList<>();

    public String getArtist() {
        return artist;
    }
    public int getLength(ArrayList<Track> tracks){
        int sumlength =0;
        for(int i = 0;i < tracks.size();i++){
            sumlength+=tracks.get(i).getLength();
        }
        super.length=sumlength;
        return super.length;
    }

    public void addTrack(Track track){
        if(tracks.contains(track)){
            System.out.println("Already exist");
        }else{
            tracks.add(track);
        }
    }

    public void  removeTrack(Track track){
        if((tracks.contains(track))){
            tracks.remove(track);
        }
    }

    @Override
    public void play() {
        for( int i =0 ; i< tracks.size() ; i++){
            tracks.get(i).play();
        }
    }

    @Override
    public int compareTo(Object o) {
        return this.artist.compareTo(((CompactDisc)o).getArtist());
    }
}
