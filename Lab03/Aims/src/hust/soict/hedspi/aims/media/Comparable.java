package hust.soict.hedspi.aims.media;

public interface Comparable {
    public int compareTo(Object obj);
}
