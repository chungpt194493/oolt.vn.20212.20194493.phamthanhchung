package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.order.Order;

public class DiskTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Order anOrder = Order.initOrder();
		
		// Create a new dvd object and set the fields
		DigitalVideoDisc dvd1 = new DigitalVideoDisc ("The Lion King");
		dvd1.setCategory ("Animation");
		dvd1.setCost (19.95f);
		dvd1.setDirector ("Roger Allers");
		dvd1.setLength (87);
		anOrder.addMedia(dvd1);
		System.out.println(dvd1.getDirector());
		
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory ("Sciece Fiction");
		dvd2.setCost (24.95f);
		dvd2.setDirector ("Geogre Lucas");
		dvd2.setLength (124);
		anOrder.addMedia(dvd2);
		
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin Here that");
		dvd3.setCategory ("Animation");
		dvd3.setCost (18.99f);
		dvd3.setDirector ("John Musker");
		dvd3.setLength (90);
		anOrder.addMedia(dvd3);
		
		
		System.out.print("Total cost is: ");
		System.out.println(anOrder.totalCost());
		
		
		Media freedisk = anOrder.getALuckyItem();
		System.out.println("Free disk: " + freedisk.getTitle());
		
		System.out.print("Total cost is: ");
		System.out.println(anOrder.totalCost());
		anOrder.print();
		
		System.out.println(dvd3.search("Aladdin"));
		
	}

}
