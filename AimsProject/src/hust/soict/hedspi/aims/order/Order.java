package hust.soict.hedspi.aims.order;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.naming.LimitExceededException;

import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.utils.MyDate;

public class Order {
	public static final int MAX_NUMBER_ORDERS = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0; 
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private MyDate dateOrdered;
	
	public void addMedia(Media media) {
		if (!itemsOrdered.contains(media)) {
			itemsOrdered.add(media);
		}
	}
	
	
	public void removeMedia(Media media) {
		if (itemsOrdered.contains(media)) {
			itemsOrdered.remove(media);
		}
	}
	
	public void removeMediaId(int id) {
		Iterator<Media> iter = itemsOrdered.iterator();
		int temp = 0;
		while (iter.hasNext())
		{
			Media mediaItem = iter.next();
			if (mediaItem.getId() == id)
			{
				iter.remove();
				temp = 1;
				System.out.println("Delete success");
			}
		}
		if (temp == 0)
			throw new NullPointerException("Order doens't contain this ID");
	}
	
	public Order() throws LimitExceededException {
		nbOrders++;
		if(nbOrders< MAX_LIMITTED_ORDERS) {
			// TODOSet initial values for object attributes
			dateOrdered = new MyDate();
		} 
		else { 
			throw new LimitExceededException("ERROR: The number of orders has reached its limit!");}
		}
		
	
	public Order(String day, String month, String year) throws LimitExceededException {
		nbOrders++;
		if(nbOrders< MAX_LIMITTED_ORDERS) {
			dateOrdered = new MyDate(day,month,year);
		} 
		else { 
			throw new LimitExceededException("ERROR: The number of orders has reached its limit!");
		}
		
	}

	
	public static Order initOrder(){
		if (nbOrders + 1 <= MAX_LIMITTED_ORDERS) {
			System.out.println("Initialize success! Number of orders: " + (nbOrders+1));
			try {
				return new Order();
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println(e.getMessage());
			}
		}
		else {
			System.out.println("Error! Limitted order");
			return null;
		}
		return null;
	}

	public int getNbOrders() {
		return nbOrders;
	}

	public int getMaxLimittedOrders() {
		return MAX_LIMITTED_ORDERS;
	}
	
	public ArrayList<Media> getItemsOrdered() {
		return itemsOrdered;
	}

	public float totalCost() {
		float totalCost = 0.0f;
		Media mediaItem;
		Iterator<Media> iter = itemsOrdered.iterator();
		while (iter.hasNext()) {
			mediaItem = (Media) (iter.next());
			totalCost += mediaItem.getCost();
		}
		return totalCost;
	}
	
	public Media getALuckyItem() {
		Random random = new Random(); 
		int rand = random.nextInt(itemsOrdered.size());
		itemsOrdered.get(rand).setCost(0);
		return itemsOrdered.get(rand);
	}
	
	public void print() {
		System.out.println("***********************Order***********************");
		System.out.println("Date: " + dateOrdered.getDay() + "/" + dateOrdered.getMonth() + "/" + dateOrdered.getYear());
		System.out.println("Ordered Items:");
		Media mediaItem;
		Iterator<Media> iter = itemsOrdered.iterator();
		while (iter.hasNext()) {
			mediaItem = (Media) (iter.next());
			System.out.println("DVD - " + mediaItem.getTitle()
					  + " - " + mediaItem.getCategory() 
//					  + " - " + mediaItem.getDirector()
//					  + " - " + mediaItem.getLength()
					  + " - " + mediaItem.getCost());
		}
		System.out.println("Total cost: " + this.totalCost());
		System.out.println("***************************************************");
	}
}
