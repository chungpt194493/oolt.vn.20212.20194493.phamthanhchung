package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Book extends Media {
	
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String,Integer> wordFrequency = new TreeMap<String,Integer>();
	
	public void addAuthor(String authorName) {
		if (!authors.contains(authorName)) {
			authors.add(authorName);
		}
	}
	
	public void removeAuthor(String authorName) {
		if (authors.contains(authorName)) {
			authors.remove(authorName);
		}
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void processContent()
	{
		this.contentTokens = Arrays.asList(this.content.split("\\s+"));  
		Collections.sort(contentTokens);
		for (String s: contentTokens)
	        {
	            Integer count = wordFrequency.get(s);
	            if (count == null) {
	                count = 0;
	            }
	            wordFrequency.put(s, count + 1);
	        }
	}
	
	@Override
	public String toString() {
		String temp ;
		temp = this.title + " "+ this.category +" "+ this.cost +" " +this.id +" "+ this.contentTokens.size() + " ";
		for (String author: authors)
			temp += author + " ";
		for (Map.Entry<String, Integer> entry: wordFrequency.entrySet()) {
	            temp += entry.getKey() + ": " + entry.getValue()+ " ";
	        }
			
		return temp;
	}
	
	// Constructor
	public Book() {
		// TODO Auto-generated constructor stub
	}
	
	public Book(String title) {
		super(title);
	}
	
	public Book(String title, String category) {
		super(title, category);
	}
	
	public Book(String title, String category, List<String> authors){
		super(title, category);
		this.authors = authors;
		//TODO: check author condition
	}
	
	
	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}
	
	public Book(String title, String category, float cost, int id) {
		super(title, category, cost, id);
	}

	public Book(String title, String category, float cost, int id, List<String> authors) {
		super(title, category, cost, id);
		this.authors = authors;
	}

}
