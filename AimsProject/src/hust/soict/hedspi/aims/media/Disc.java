package hust.soict.hedspi.aims.media;


public class Disc extends Media {
	
	protected String director;
	protected int length;

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public boolean setLength(int length) {
		if (length > 0) {
			this.length = length;
			return true;
		}
		else return false;
	}
	

	public Disc() {
		// TODO Auto-generated constructor stub
	}
	
	public Disc(String title) {
		super(title);
	}
	
	public Disc(String title, String category) {
		super(title, category);
	}
	
	public Disc(String title, String category, float cost){
		super(title, category, cost);
	}
	
	public Disc(String title, String category, float cost, int id) {
		super(title, category, cost, id);
		// TODO Auto-generated constructor stub
	}
}
