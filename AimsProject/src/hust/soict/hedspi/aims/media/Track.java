package hust.soict.hedspi.aims.media;

import hust.soict.hedspi.aims.PlayerException;

public class Track implements Playable{
	
	private String title;
	private int length;
	
	public String getTitle() {
		return title;
	}

	public int getLength() {
		return length;
	}
	
//	public void play() {
//		System.out.println("Playing DVD: " + this.getTitle());
//		System.out.println("DVD length: " + this.getLength()); 
//	}
//	
	public void play() throws PlayerException { 
		if (this.getLength() > 0) {
			// TODO Play DVD as you have implemented 
			System.out.println("Playing DVD: " + this.getTitle());
			System.out.println("DVD length: " + this.getLength()); 
		} 
		else {
			throw new PlayerException("ERROR: DVD length is non-positive!");
		} 
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Track) {
			Track track = (Track) obj;
			return this.title == track.title && this.length == track.length;
		}
		return false;
	}
	
//    Constructor
	public Track() {
		// TODO Auto-generated constructor stub
	}
	
	public Track(String title) {
		super();
		this.title = title;
		this.length = 1;
	}

	public Track(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}
}
