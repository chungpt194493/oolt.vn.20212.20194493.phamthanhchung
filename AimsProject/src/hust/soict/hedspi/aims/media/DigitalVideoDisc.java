package hust.soict.hedspi.aims.media;

import hust.soict.hedspi.aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable{

	private float cost;	
	
	public float getCost() {
		return cost;
	}
	
	public void setCost(float cost) {
		this.cost = cost;
	}
	
	
	public boolean search(String title) {
		String titleStrings[] = title.split(" ");
		int check = 0;
 		for (String titleString : titleStrings) {
 			if (this.title.contains(titleString)) {
 				check++;
 				continue;
 			}
		}
 		if (check == titleStrings.length) {
 			return true;
 		}
 		return false;
	}
	
	public void play() throws PlayerException { 
		if (this.getLength() > 0) {
			// TODO Play DVD as you have implemented 
			System.out.println("Playing DVD: " + this.getTitle());
			System.out.println("DVD length: " + this.getLength()); 
		} 
		else {
			throw new PlayerException("ERROR: DVD length is non-positive!");
		} 
	}
	
	
	// Constructor
	public DigitalVideoDisc() {
		this.title = "noname";
		this.category = "unknown";
		this.cost = 0.0f;
		this.director = "unknown";
		this.length = 0;
		
	}
	
	public DigitalVideoDisc(String title, String category, float cost, int id, String director) {
		super(title, category, cost, id);
		this.director = director;
	}
	

	public DigitalVideoDisc(String title, String category, float cost, int id, String director, int length) {
		super(title, category, cost, id);
		this.director = director;
		this.length = length;
	}
	
	public DigitalVideoDisc(String title, String category, float cost, int id) {
		super(title, category, cost, id);
	}
	
	
	public DigitalVideoDisc(String title, String category, float cost) {
		super(title, category, cost);
	}

	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}

	public DigitalVideoDisc(String title) {
		super(title);
	}

}
