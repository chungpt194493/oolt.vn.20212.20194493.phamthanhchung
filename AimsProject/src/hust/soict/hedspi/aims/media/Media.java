package hust.soict.hedspi.aims.media;

public abstract class Media implements Comparable<Object>{
	
	protected String title;
	protected String category;
	protected float cost;
	protected int id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Media) {
			Media m = (Media) obj;
			return this.id == m.id;
		}
		return false;
	}
	
	public int compareTo(Object obj) {
		if(obj instanceof Media) {
			Media media = (Media) obj;
			return media.title.compareTo(this.title);
		}
		return -9999;
	}

//	Constructor
	public Media() {
		// TODO Auto-generated constructor stub
	}
	
	public Media(String title) {
		this.title = title;
	}
	
	public Media(String title, String category) {
		this.title = title;
		this.category = category;
	}
	
	public Media(String title, String category, float cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	
	public Media(String title, String category, float cost, int id ){	
		this(title);
		this.category = category;
		this.cost  = cost;
		this.id = id;
	}
}
