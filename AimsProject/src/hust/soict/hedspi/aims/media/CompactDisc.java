package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hust.soict.hedspi.aims.PlayerException;

public class CompactDisc extends Disc implements Playable{
	

	private String artist;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	
	public void addTrack(Track track) throws  NullPointerException {
		if (tracks.add(track) == false)
			throw new NullPointerException("Cannot add");
		this.length += track.getLength();
	}
	
	public void removeTrack(Track track) {
		if (tracks.contains(track)) {
			tracks.remove(track);
		}
	}

	public ArrayList<Track> getTracks() {
		return tracks;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getArtist() {
		return artist;
	}
	
	@Override
	public int getLength() {
		int length = 0;
		Iterator<Track> iter = tracks.iterator();
		while(iter.hasNext()) {
			Track trackItem = iter.next();
			length += trackItem.getLength();
		}
		return length;
	}
	
	
//	
//	public void play() {
//		Iterator<Track> iter = tracks.iterator();
//		while(iter.hasNext()) {
//			Track trackItem = iter.next();
//			trackItem.play();
//		}
//	}
	
	public void play() throws PlayerException{ 
		if(this.getLength() > 0) {
			// TODO Play all tracks in the CD as you have implemented 
			java.util.Iterator iter = tracks.iterator();
			Track nextTrack;
			while(iter.hasNext()) {
				nextTrack = (Track) iter.next(); 
				try {
					nextTrack.play(); 
				}
				catch(PlayerException e) {
					throw e; 
				}
			} 
		}
		else {
			throw new PlayerException("ERROR: CD length is non-positive!");
		}
	}
	
	@Override
	public int compareTo(Object obj) {
		if (obj instanceof CompactDisc) {
			CompactDisc Cd = (CompactDisc) obj;
			if (this.title.equals(Cd.getTitle())) {
				if (this.length < Cd.getLength())
					return -1;
				else if (this.length > Cd.getLength())
					return 1;
				else return 0;
			}
			return this.title.compareTo(Cd.getTitle());
		}
		return -9999;
	}
	
	// Constructor
	public CompactDisc() {
		// TODO Auto-generated constructor stub
	}
	
	public CompactDisc(String title, String category, float cost, int id, String director, String artist) {
		super(title, category, cost, id);
		this.director = director;
		this.artist = artist;
	}
	
	public CompactDisc(String title, String category, float cost, int id) {
		super(title, category, cost, id);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category, float cost) {
		super(title, category, cost);
	}

	public CompactDisc(String title, String category) {
		super(title, category);
	}

	public CompactDisc(String title) {
		super(title);
	}
}
