package hust.soict.hedspi.aims;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;


import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.MemoryDeamon;
import hust.soict.hedspi.aims.media.Track;
import hust.soict.hedspi.aims.order.Order;

public class Aims {
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------"); 
		System.out.println("1. Create new order"); 
		System.out.println("2. Add item to the order"); 
		System.out.println("3. Delete item by id"); 
		System.out.println("4. Display the items list of order"); 
		System.out.println("0. Exit"); 
		System.out.println("--------------------------------"); 
		System.out.println("Please choose a number: 0-1-2-3-4");
		
	}
	
	public static void main(String[] args) {
		int choose;
		Scanner sc = new Scanner(System.in);
		Order anOrder = Order.initOrder();
		MemoryDeamon deamon = new MemoryDeamon();
		Thread thread = new Thread(deamon);
		thread.setDaemon(true);
		do {
			showMenu();
			choose = sc.nextInt();
			switch (choose) {
				case 0:
					System.out.println("Exit!");
					break;
				case 1:
					anOrder = Order.initOrder();
					break;
				case 2:
					int choice;
					do {
						System.out.println("--------------------------------"); 
						System.out.println("Choose type of item: ");
						System.out.println("1. Book"); 
						System.out.println("2. Compact Disc"); 
						System.out.println("3. Digital Video Disc");
						System.out.println("0. Exit"); 
						choice = sc.nextInt();
						switch (choice){
							case 1: {
								Book book1 = new Book("Sach ne");
								Book book2 = new Book("Book", "Category");
								List<String> authors = new ArrayList<String>();
								authors.add("AN");
								Book book3 = new Book("Sach", "Category", authors);
								anOrder.addMedia(book1);
								anOrder.addMedia(book2);
								anOrder.addMedia(book3);
								break;
							}
							case 2: {
								CompactDisc dvd1 = new CompactDisc("Star Wars");
								Track track1 = new Track("Track1");
								dvd1.addTrack(track1);
								Track track2 = new Track("Track2", 3);
								dvd1.addTrack(track2);
//								dvd2.setCategory ("Sciece Fiction");
//								dvd2.setCost (24.95f);
//								dvd2.setDirector ("Geogre Lucas");
//								dvd2.setLength (124);
								dvd1.getLength();
								System.out.println("Do you want to play: 1. Yes\t2.No");
								int playchoice = sc.nextInt();
								if(playchoice == 1) {
									try {
										dvd1.play();
									}
									catch (Exception e) {
										// TODO: handle exception
										System.out.println(e.getMessage());
										e.printStackTrace();
									}
								}
								anOrder.addMedia(dvd1);
								break;
							}
							case 3: {
								DigitalVideoDisc dvd2 = new DigitalVideoDisc();
								dvd2.setTitle("The Lion King");
								dvd2.setCategory("Animation");
								dvd2.setCost(19.95f);
								dvd2.setDirector("Roger Allers");
								dvd2.setLength(-3);
								System.out.println("Do you want to play: 1. Yes\t2.No");
								int playchoice = sc.nextInt();
								if(playchoice == 1) {
									try {
										dvd2.play();
									}
									catch (Exception e) {
										 System.out.println(e.getMessage());
									}
								}
								anOrder.addMedia(dvd2);
								break;
							}
							case 0:{
								System.out.println("Exit !");
							}
						}
					} while(choice != 0);
					break;
				case 3:
					System.out.println("Nhap id muon xoa");
					int id = sc.nextInt();
					anOrder.removeMediaId(id);
					System.out.println();
					break;
				case 4:
					anOrder.print();
					System.out.println("\nTotal Cost: " + anOrder.totalCost());
					break;
				default:
					System.out.println("Try again!");
					break;
			}
		} while (choose != 0);
		deamon.run();
		sc.close();
	}
	
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		Order anOrder = Order.initOrder();
//		
//		// Create a new dvd object and set the fields
//		DigitalVideoDisc dvd1 = new DigitalVideoDisc ("The Lion King");
//		dvd1.setCategory ("Animation");
//		dvd1.setCost (19.95f);
//		dvd1.setDirector ("Roger Allers");
//		dvd1.setLength (87);
//		// Add the dvd to the order
//		anOrder.addDigitalVideoDisc(dvd1);
//		
//		
//		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
//		dvd2.setCategory ("Sciece Fiction");
//		dvd2.setCost (24.95f);
//		dvd2.setDirector ("Geogre Lucas");
//		dvd2.setLength (124);
//		// Add the dvd to the order
//		anOrder.addDigitalVideoDisc(dvd2);
//		
//		
//		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
//		dvd3.setCategory ("Animation");
//		dvd3.setCost (18.99f);
//		dvd3.setDirector ("John Musker");
//		dvd3.setLength (90);
//		// Add the dvd to the  order
//		anOrder.addDigitalVideoDisc(dvd3);
//		
//		
//		System.out.print("Total cost is: ");
//		System.out.println(anOrder.totalCost());
//		
//		
//		// Test remove
//		anOrder.removeDigitalVideoDisc(dvd3);
//		
//		
//		System.out.print("Total cost is: ");
//		System.out.println(anOrder.totalCost());
//		anOrder.print();
//		
//	}

}
