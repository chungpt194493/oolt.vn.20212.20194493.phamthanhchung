package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.order.Order;

import java.security.spec.RSAOtherPrimeInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Aims {
    static Order order;
    public static void main (String[] args ) {
        DigitalVideoDisc dvd0 = new DigitalVideoDisc();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation ");
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star War","Science","George Lucas ", 124,24.95f);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation ","John Musker",98,18.95f);
        dvd0.printInfor();
        dvd1.printInfor();
        dvd2.printInfor();
        dvd3.printInfor();

//        Order anOrder = new Order();
//        anOrder.addDigitalVideoDisc(dvd1);
//        anOrder.addDigitalVideoDisc(dvd2);
//        anOrder.addDigitalVideoDisc(dvd3);
//
//        System.out.println("Total cost "+ anOrder.totalCost());
//        anOrder.removeDigitalVideoDisc(dvd3);
//        System.out.println("Total cost "+ anOrder.totalCost());
        while (true) showMenu();
    }

    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Additem to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
        Scanner sc = new Scanner(System.in);
        int choose = sc.nextInt();
        switch (choose) {
            case 0 :
                System.out.println("Exit");
                System.exit(0);
            case 1 :
                order = Order.createOrder();
                System.out.println("Created new order ");
                break;
            case 2 :
                if (order == null) {
                    System.out.println("Bạn cần tạo đơn hàng trước.");
                } else {
                    System.out.println("""
                            Chọn loại item:
                            1.Book
                            2.CompactDisc(chưa có gì)
                            3.DigitalVideoDisc
                            """);
                    int case2 = sc.nextInt();
                    switch (case2){
                        case 1 -> {
                            System.out.println("Nhập title Book: ");
                            sc = new Scanner(System.in);
                            String title = sc.nextLine();
                            System.out.println("Nhập Category: ");
                            String category = sc.nextLine();
                            System.out.println("Nhập Author: ");
                            List<String> authors = new ArrayList<>();
                            order.addMedia(new Book(title,category,authors));
                        }

                        case 3 -> {
                            System.out.println("Nhập title DigitalVideoDisc: ");
                            sc = new Scanner(System.in);
                            String title = sc.nextLine();
                            System.out.println("Nhập Category: ");
                            String category = sc.nextLine();
                            System.out.println("Nhập Director: ");
                            String director = sc.nextLine();
                            order.addMedia(new DigitalVideoDisc(title,category,director));
                        }
                        default -> {
                            System.out.println("lỗi");
                            System.exit(1);
                        }

                    }

                }
                break;
            case 3 :
                if (order == null) {
                    System.out.println("Bạn cần tạo đơn hàng trước.");
                } else {
                    order.show();
                    System.out.println("Nhập id: ");
                    order.removeMedia(sc.nextInt());
                    System.out.println("Xóa thành công.");
                }
                break;
            case 4 :
                System.out.println("Danh sách sản phẩm trong đơn hàng");
                order.show();
                break;
            default:
                System.out.println("Lỗi lựa chọn.");
        }
    }
}
