package hust.soict.hedspi.gui.awt;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AWTCounter extends Frame {
    // khai bao component giao dien
    private Label lblCount;
    private TextField tfCount;
    private Button btnCount;
    private int count;

    public AWTCounter(){
        // thiet lap layout cho cửa sổ giao diện
        // FlowLayout : bo cuc dang dơn giản : component nào duoc them vao truoc se xuat hien trươc tren 1 hàng , neu k du
        // thi xuong dong
        this.setLayout(new FlowLayout());
        lblCount = new Label("Counter");
        this.add(lblCount);

        tfCount = new TextField(count + "", 10);// construct the TextField component with initial text//
        tfCount.setEditable(false);      // set to read-only
        this.add(tfCount);

        btnCount = new Button("Count ");
        this.add(btnCount);

        //dăng ki lang nghe su kien tren button

        btnCount.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ++count;
                        tfCount.setText(count + "");
                    }
                }
        );

        // Thiet lap thong tin cho cua so giao dien
        this.setTitle("AWT Counter");
        this.setSize(250, 100);
        this.setVisible(true);

    }

    public static void main(String[] args){
        AWTCounter app = new AWTCounter();
    }


}

