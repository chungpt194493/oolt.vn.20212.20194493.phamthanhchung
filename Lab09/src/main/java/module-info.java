module hust.soict.hedspi.gui.javafx {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;


    opens hust.soict.hedspi.gui to javafx.fxml;
    exports hust.soict.hedspi.gui;
    exports hust.soict.hedspi.gui.javafx;
    opens hust.soict.hedspi.gui.javafx to javafx.fxml;
}