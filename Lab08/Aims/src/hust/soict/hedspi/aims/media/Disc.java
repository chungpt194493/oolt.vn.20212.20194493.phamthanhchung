package hust.soict.hedspi.aims.media;

public class Disc extends Media{
    protected String director;
    protected int length;

    public String getDirector() {
        return director;
    }


    public int getLength() {
        return length;
    }


    public Disc() {
       this.director="";
       this.length=0;
    }

    public Disc(int length, String director) {
        super();
        this.length = length;
        this.director = director;
    }



}
