package hust.soict.hedspi.aims.media;

import java.sql.SQLOutput;

public class DigitalVideoDisc extends Disc implements Playable ,Comparable {

    private String director;
    private int length;


    // Xay dung cac phuong thuc


    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
            return length;
    }

    public void setLength(int length) {
        if (length >= 0)
            this.length = length;
        else
            this.length=0;
    }


    //2. Cac phuong thuc khoi tao
    // Nv : Tao vùng nhớ , chứa thông tin của object và thiết lập // gán giá trị cho các thuộc tính của object
    // Đặc điểm của constructor
    // tên của constructor trùng với tên lớp
    // ko có kiểu trả về , ko dùng từ khóa void

    //2.1 : Constructor ko tham số
    public DigitalVideoDisc(){
        this.title = "";
        this.category="";
        this.director="";
        this.length=0;
        this.cost=0.0f;
    }

    //2.2 Constructor 1 tham so
    public DigitalVideoDisc(String title) {
        super();
        this.setTitle(title);
    }
    public DigitalVideoDisc(String title, String category, String director) {
        super();
        this.setTitle(title);
        this.setCategory(category);
        this.director = director;
    }
    // Constructor full tham so
    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super();
        this.setTitle(title);
        this.setCategory(category);
        this.director = director;
        this.length = length;
        this.setCost(cost);
    }

    //Cac phuong thuc khac
    // in ra thong tin cua doi tuong DVD
    public void printInfor(){
        System.out.println(" -----------DVD Infor-------------- ");
        System.out.println("Title : " + this.title);
        System.out.println("Category: " + this.category);
        System.out.println("Director: " + this.director);
        System.out.println("Length: "+ this.length);
        System.out.println("Cost :"+ this.cost);
    }

    //phuong thuc search
        public boolean search(String title){
            int check=0;
            String sTitle[] = title.split(" ");
            for(int i=0 ; i < sTitle.length;i++){
                if(this.title.contains(sTitle[i])){
                    check =1;
                }else {
                    check =0;
                    break;
                }
            }
            if(check ==1) {
                return true ;
            }
            else return false;
        }

//    public boolean search(String title) {
//        return this.getTitle().toLowerCase().contains(title.toLowerCase());
//    }


    @Override
    public void play() {
        System.out.println("Play DVD: "+ this.getTitle());
        System.out.println("DVD length : " + this.getLength());
    }

   @Override
    public int compareTo(Object o) {
        return this.title.compareTo(((DigitalVideoDisc)o).getTitle());
    }
}
