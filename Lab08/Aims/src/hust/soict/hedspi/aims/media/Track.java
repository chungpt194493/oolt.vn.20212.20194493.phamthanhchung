package hust.soict.hedspi.aims.media;

public class Track implements Playable,Comparable {
    private int length;
    private String title;

    public int getLength() {
        return length;
    }

    public String getTitle() {
        return title;
    }

    public Track(){
        this.title="";
        this.length=0;
    }
    public  Track(String title,int length){
        this.length=length;
        this.title=title;
    }



    @Override
    public void play() {
        System.out.println("Playing Track : "+ this.getTitle());
        System.out.println("Track length : " + this.getLength());
    }

    @Override
    public boolean equals(Object obj) {
        Track track2 = (Track) obj;
        return (this.title.equals(track2.title) && this.length == track2.length);
    }

    public int compareTo(Object o){
        return this.title.compareTo(((Track)o).getTitle());
    }
}
