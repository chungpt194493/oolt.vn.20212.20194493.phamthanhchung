package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.order.Order;

public class Aims {
    public static void main (String[] args ) {
        DigitalVideoDisc dvd0 = new DigitalVideoDisc();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation ");
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star War","Science","George Lucas ", 124,24.95f);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation ","John Musker",98,18.95f);
        dvd0.printInfor();
        dvd1.printInfor();
        dvd2.printInfor();
        dvd3.printInfor();

        Order anOrder = new Order();
        anOrder.addDigitalVideoDisc(dvd1);
        anOrder.addDigitalVideoDisc(dvd2);
        anOrder.addDigitalVideoDisc(dvd3);

        System.out.println("Total cost "+ anOrder.totalCost());
        anOrder.removeDigitalVideoDisc(dvd3);
        System.out.println("Total cost "+ anOrder.totalCost());
    }

}
