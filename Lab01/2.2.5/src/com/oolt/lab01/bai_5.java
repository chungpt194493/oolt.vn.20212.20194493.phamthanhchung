package com.oolt.lab01;
import javax.swing.JOptionPane;


public class bai_5 {
    public static void main(String[] args ) {


        String strnum1, strnum2;
        strnum1 = JOptionPane.showInputDialog(null, "Please input the first number ", "Input the first number ",
                JOptionPane.INFORMATION_MESSAGE);
        strnum2 = JOptionPane.showInputDialog(null, "Please input the second number ", "Input the second number ",
                JOptionPane.INFORMATION_MESSAGE);
        double num1 = Double.parseDouble(strnum1);
        double num2 = Double.parseDouble(strnum2);
        double tong = num1 + num2;
        double hieu = num1 - num2;
        double tich = num1 * num2;
        double thuong = num1 / num2;
        JOptionPane.showMessageDialog(null, "Tong hai so la " + tong, "Tính tổng", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null, "Hieu hai so la " + hieu, "Tính hiệu ", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showMessageDialog(null, "Tich hai so la " + tich, "Tính tích", JOptionPane.INFORMATION_MESSAGE);
        if (num2 == 0) {
            JOptionPane.showMessageDialog(null, "Khong the chia duoc ", "Tính thương", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Thuong hai so la " + thuong, "Tính thương ", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
    }
}
