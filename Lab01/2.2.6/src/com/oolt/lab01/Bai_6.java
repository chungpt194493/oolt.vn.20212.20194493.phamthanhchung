package com.oolt.lab01;

import javax.swing.*;
import java.lang.Math;
public class Bai_6 {
    public static void main(String[] args) {
        // Pt bac nhat
        String strnum1, strnum2;
        strnum1 = JOptionPane.showInputDialog(null, "Nhập hệ số bậc nhất (khác 0)", "Input number ",
                JOptionPane.INFORMATION_MESSAGE);
        strnum2 = JOptionPane.showInputDialog(null, "Nhập hệ số tự do ", "Input number ",
                JOptionPane.INFORMATION_MESSAGE);
        double num1 = Double.parseDouble(strnum1);
        double num2 = Double.parseDouble(strnum2);
        double x = -num2/num1;
        JOptionPane.showMessageDialog(null, "Nghiệm duy nhất của phương trình bậc nhất là " + x, "Giải nghiệm", JOptionPane.INFORMATION_MESSAGE);


        // hệ pt bậc nhất
        String stra11,stra12,strb1,stra21,stra22,strb2;
        stra11 = JOptionPane.showInputDialog(null, "Nhập hệ số thứ nhất của phương trình 1 ", "Input number ",
                JOptionPane.INFORMATION_MESSAGE);
        stra12 = JOptionPane.showInputDialog(null, "Nhập hệ số thứ hai của phương trình 1 ", "Input number ",
                JOptionPane.INFORMATION_MESSAGE);
        strb1 = JOptionPane.showInputDialog(null, "Nhập hệ số tự do của phương trình 1 ", "Input number ",
                JOptionPane.INFORMATION_MESSAGE);
        stra21 = JOptionPane.showInputDialog(null, "Nhập hệ số thứ nhất của phương trình 1 ", "Input number ",
                JOptionPane.INFORMATION_MESSAGE);
        stra22 = JOptionPane.showInputDialog(null, "Nhập hệ số thứ nhất của phương trình 1 ", "Input number ",
                JOptionPane.INFORMATION_MESSAGE);
        strb2 = JOptionPane.showInputDialog(null, "Nhập hệ số thứ nhất của phương trình 1 ", "Input number ",
                JOptionPane.INFORMATION_MESSAGE);
        double a11 = Double.parseDouble(stra11);
        double a12 = Double.parseDouble(stra12);
        double b1 = Double.parseDouble(strb1);
        double a21 = Double.parseDouble(stra21);
        double a22 = Double.parseDouble(stra22);
        double b2 = Double.parseDouble(strb2);

        double D = a11*a22  - a12*a21;
        double D1 = b1*a22  - b2*a12;
        double D2 =  a11*b2 - a21*b1;

        double x1 = D1/D;
        double x2 = D2/D;
        if(D !=0){
            JOptionPane.showMessageDialog(null, "Cặp nghiệm của hệ phương trình là ( x1,x2 ) = (" +x1 +","+x2 +")",
                    "Kết quả ", JOptionPane.INFORMATION_MESSAGE);
        }else {
            if(D1 == 0 && D2 == 0 ){
                JOptionPane.showMessageDialog(null, "Hệ phương trình có vô số nghiệm " ,
                        "Kết quả ", JOptionPane.INFORMATION_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(null, "Hệ phương trình  vô  nghiệm " ,
                        "Kết quả ", JOptionPane.INFORMATION_MESSAGE);
            }
        }

        // pt bậc 2

        String stra,strb,strc;
        stra = JOptionPane.showInputDialog(null, "Nhập hệ số bậc 2 của phương trình  ", "Input number ",
                JOptionPane.INFORMATION_MESSAGE);
        strb = JOptionPane.showInputDialog(null, "Nhập hệ số bậc 1 của phương trình  ", "Input number ",
                JOptionPane.INFORMATION_MESSAGE);
        strc = JOptionPane.showInputDialog(null, "Nhập hệ số  tự do của phương trình  ", "Input number ",
                JOptionPane.INFORMATION_MESSAGE);
        double a = Double.parseDouble(stra);
        double b = Double.parseDouble(strb);
        double c = Double.parseDouble(strc);
        double denta = b*b -4*a*c;

        double x3 = -b/2*a;
        if ( denta ==0 ){
            JOptionPane.showMessageDialog(null, "Phương trình có 1 nghiệm duy nhất x =  " + x3 ,
                    "Kết quả ", JOptionPane.INFORMATION_MESSAGE);
        }else{
            if(denta > 0 ){
                double x4 = ( -b + Math.sqrt(denta) )/2*a ;
                double x5 = ( -b - Math.sqrt(denta) )/2*a ;
                JOptionPane.showMessageDialog(null, "Phương trình có 2 nghiệm phân biệt x1 =" + x4 + "và x2 = " + x5 ,
                        "Kết quả ", JOptionPane.INFORMATION_MESSAGE);
            }else {
                JOptionPane.showMessageDialog(null, "Hệ phương trình  vô  nghiệm " ,
                        "Kết quả ", JOptionPane.INFORMATION_MESSAGE);

            }
        }
    }
}
