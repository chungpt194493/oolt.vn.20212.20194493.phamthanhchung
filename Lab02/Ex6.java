package Lab02;

import java.util.Scanner;
import java.util.Arrays;

public class Ex6 {
    public static void main(String[] args) {
        int n;
        double sum = 0;
        double average = 0;
        Scanner s = new Scanner(System.in);
        System.out.print(" Nhap vao so phan tu cua mang:");
        n = s.nextInt();

        double arr[] = new double[n];
        for(int i = 0; i < n ; i++)
        {
            System.out.print("Nhap vao phan tu thu " + (i + 1) + " : ");
            arr[i] = s.nextDouble();
            sum = sum + arr[i];
        }

        Arrays.sort(arr);
        System.out.print("\nArr sau khi sort: ");
        for(int i = 0; i < n ; i++)
        {
            System.out.print(arr[i] + " ");
        }

        average = (double)sum / n;
        System.out.println("\nSum:"+sum);
        System.out.println("Average:"+average);
    }
}
