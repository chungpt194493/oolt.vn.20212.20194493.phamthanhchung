package Lab02;

import java.util.Scanner;

public class Ex7 {

    public static void main(String[] args) {

        int r, c, i, j;
        Scanner s = new Scanner(System.in);

        System.out.print("Nhap so dong: ");
        r = s.nextInt();

        System.out.print("Nhap so cot: ");
        c = s.nextInt();

        double mt1[][] = new double[r][c];
        double mt2[][] = new double[r][c];
        double sum[][] = new double[r][c];

        System.out.println("Nhap ma tran 1:");
        for (i = 0; i < r; i++) {
            for (j = 0; j < c; j++) {
                System.out.print("Nhap vao a[" + i + "]" + "[" + j + "]: ");
                mt1[i][j] = s.nextDouble();
            }
        }

        System.out.println("Nhap ma tran 2:");
        for (i = 0; i < r; i++) {
            for (j = 0; j < c; j++) {
                System.out.print("Nhap vao b[" + i + "]" + "[" + j + "]: ");
                mt2[i][j] = s.nextDouble();
            }
        }
        System.out.println("Ket qua phep cong 2 ma tran: ");
        for (i = 0; i < r; i++) {
            for (j = 0; j < c; j++) {
                sum[i][j] = mt1[i][j] + mt2[i][j];
                System.out.printf("%.2f\t", sum[i][j]);
            }
            System.out.print("\n");
        }
    }
}
