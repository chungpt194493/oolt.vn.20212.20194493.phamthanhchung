package Lab02;

import java.util.Scanner;
import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Ex5 {
    public static boolean checkMonth(String month) {
        return true;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int choPhep = 0;
        int laplai = 0;

        int number_Of_Days = 0;
        String month = "";
        String Str_year = "";
        int year = 0;

        do {
            System.out.print("Nhập vào năm : ");
            Str_year = input.nextLine();
            Pattern pattern = Pattern.compile("\\d*");
            Matcher matcher = pattern.matcher(Str_year);
            if (matcher.matches()) {
                year = Integer.parseInt(Str_year);
                choPhep = 1;
            } else {
                System.out.println("Ban nhap sai dinh dang nam, hay nhap lai !");
            }

        } while (choPhep == 0);

        do {
            System.out.print("Nhap vao thang: ");
            month = input.nextLine();
            switch (month) {
                case "January":
                case "Jan":
                case "Jan.":
                case "1":
                    number_Of_Days = 31;
                    laplai = 0;
                    break;
                case "February":
                case "Feb":
                case "Feb.":
                case "2":
                    if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
                        number_Of_Days = 29;
                    } else {
                        number_Of_Days = 28;
                    }
                    laplai = 0;
                    break;
                case "March":
                case "Mar":
                case "Mar.":
                case "3":
                    number_Of_Days = 31;
                    laplai = 0;
                    break;
                case "April":
                case "Apr":
                case "Apr.":
                case "4":
                    number_Of_Days = 30;
                    laplai = 0;
                    break;
                case "May":
                case "5":
                    number_Of_Days = 31;
                    laplai = 0;
                    break;
                case "Jun":
                case "June":
                case "6":
                    number_Of_Days = 30;
                    laplai = 0;
                    break;
                case "July":
                case "Jul":
                case "7":
                    number_Of_Days = 31;
                    laplai = 0;
                    break;
                case "August":
                case "Aug":
                case "Aug.":
                case "8":
                    number_Of_Days = 31;
                    laplai = 0;
                    break;
                case "September":
                case "Sept":
                case "Sep.":
                case "9":
                    number_Of_Days = 30;
                    laplai = 0;
                    break;
                case "October":
                case "Oct.":
                case "Oct":
                case "10":
                    number_Of_Days = 31;
                    laplai = 0;
                    break;
                case "November":
                case "Nov.":
                case "Nov":
                case "11":
                    number_Of_Days = 30;
                    laplai = 0;
                    break;
                case "December":
                case "Dec.":
                case "Dec":
                case "12":
                    number_Of_Days = 31;
                    laplai = 0;
                    break;
                default:
                    System.out.println("Vui long nhap lai thang dung dinh dang");
                    laplai = 1;
            }
        } while (laplai == 1);
        System.out.println("So ngay: " + number_Of_Days);
    }

}
